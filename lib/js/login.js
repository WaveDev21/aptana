$(document).delegate("#loginButton", "click", function() {
	var login = $("#name").val();
	var pass = $("#pass").val();
	
	
	$.ajax({
		type: "POST",
		url: "http://localhost/spawim",
		data: "login="+login+"&pass="+pass,
		beforeSend: function(){
		$("#messages").hide("slow");
	  	},
		success: function(result){
			if(result == "filled"){
				$("#messages").text(result);
				$("#messages").show("slow");
			}else if(result == "nauczyciel" || result == "uczen"){
				var type = result;
				saveUser(login, pass, type);
				
				mainLogin = login;
				mainType = type;
				$("#content").fadeOut("slow","linear");
				setTimeout(function(){
					if(mainType == "nauczyciel"){
						$("#content").load('views/teacherMenu.html');
					}else{
						$("#content").load('views/userMenu.html');
					}
				}, 600);
				$("#content").fadeIn("slow","linear");
			}
		},
		error: function(xhr, ajaxOptions, thrownError){
			alert("asdas");
			$("#menuLabel label").text("MENU (offline)");
			tmp = isUser(login, pass);
			if(tmp){
				mainType = tmp;
				mainLogin = login;
				$("#content").fadeOut("slow","linear");
				setTimeout(function(){
					$("#content").load('views/offlineMenu.html');
				}, 600);
				$("#content").fadeIn("slow","linear");
			}else{
				$("#messages").text("B��dne dane logowania");
				$("#messages").show("slow");
			}
		}
	});
});

$(document).delegate("#registerLink", "click", function() {
	$("#content").load('views/register.html');
});

function saveUser(name, pass, type){
	var con = new air.SQLConnection();
	var dbFile = air.File.applicationStorageDirectory.resolvePath('baza.db');
	con.open(dbFile);
	
	var stmt = new air.SQLStatement();
	stmt.sqlConnection = con;
	stmt.text = "CREATE TABLE IF NOT EXISTS users (" +    
	    "name TEXT, " +  
	    "pass TEXT, " +
	    "type TEXT )";
	stmt.execute();
	
	var selectStmt = new air.SQLStatement(); 
	selectStmt.sqlConnection = con;  
	var sql =  
	    "SELECT name " +  
	    "FROM users"; 
	selectStmt.text = sql; 
	try 
	{ 
	    // execute the statement 
	    selectStmt.execute(); 
	    // access the result data 
	    var result = selectStmt.getResult(); 
	    var numRows = result.data.length; 
	    for (var i = 0; i < numRows; i++) 
	    {
	       if(result.data[i]["name"] == name){

	    	   return null;
	       }
	    }
	} 
	catch (error) 
	{ 
	    alert("Error message:"+ error.message); 
	    alert("Details:"+ error.details); 
	} 
	
	
	var insertStmt = new air.SQLStatement(); 
	insertStmt.sqlConnection = con; 
	var sql =  
	    "INSERT INTO users (name, pass, type) " +  
	    "VALUES ('"+name+"', '"+pass+"', '"+type+"')"; 
	insertStmt.text = sql; 
	try 
	{ 
	    // execute the statement 
	    insertStmt.execute(); 
	    //alert("INSERT statement succeeded"); 
	} 
	catch (error) 
	{ 
	    alert("Error message:"+ error.message); 
	    alert("Details:"+ error.details); 
	}
	con.close();
	
}

function isUser(name, pass){
	var con = new air.SQLConnection();
	var dbFile = air.File.applicationStorageDirectory.resolvePath('baza.db');
	con.open(dbFile);
	
	var selectStmt = new air.SQLStatement(); 
	selectStmt.sqlConnection = con;  
	var sql =  
	    "SELECT name, pass, type " +  
	    "FROM users"; 
	selectStmt.text = sql; 
	try 
	{ 
	    // execute the statement 
	    selectStmt.execute(); 
	    // access the result data 
	    var result = selectStmt.getResult(); 
	    var numRows = result.data.length; 
	    for (var i = 0; i < numRows; i++) 
	    {
	       if(result.data[i]["name"] == name && result.data[i]["pass"] == pass){
	    	  
	    	   return result.data[i]["type"];
	       }
	    }
	    return false;
	} 
	catch (error) 
	{ 
	    alert("Error message:"+ error.message); 
	    alert("Details:"+ error.details); 
	} 
	
	con.close()
}

