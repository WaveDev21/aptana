
$(document).delegate("#widomości", "click", function() {
	$("#content").load('views/messages.html');
});

$(document).delegate("#zgłoszenia", "click", function() {
	$("#content").load('views/applications.html');
});

$(document).delegate("#oceny", "click", function() {
	$("#content").load('views/assessments.html');
});

$(document).delegate("#grafik", "click", function() {
	$("#content").load('views/chart.html');
});

				