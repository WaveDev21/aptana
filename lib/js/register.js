
$(document).delegate("#login", "keyup", function() {
	var length = $("#login").val().length;
	
	if(length>=3){
		$.ajax({
			type: "POST",
			url: "http://localhost/spawim/register/loginHint",
			data: "login="+$("#login").val(),
			success: function(result){
				if(result == "taken"){
					$("#login").attr("properlySet", "");
					$("#login_msg").css("border-width", "5px");
					$("#login_msg").css("background", "#EAEDF2");
					$("#login_msg").show("slow");
					$("#login_msg").text("Login zaj�ty");
				}else{
					$("#login").attr("properlySet", "true");
					$("#login_msg").css("border-width", "0px");
					$("#login_msg").css("background-color", "Transparent");
					$("#login_msg").html('<img src="icons/OK.png">');
				}
			}
		});
		
	}else{
		$("#login").attr("properlySet", "");
		$("#login_msg").css("border-width", "5px");
		$("#login_msg").css("background", "#EAEDF2");
		$("#login_msg").show("slow");
		$("#login_msg").text("Zbyt krutka nazwa");
	}
	
	isAllSet();
	
});

$(document).delegate("#typ", "change", function() {
	if($("#typ").val() == "nauczyciel"){
		$(".teacher").show("slow");
	}else{
		$(".teacher").hide("slow");
		$("#oferta").val("");
		$(".instrument").removeAttr('checked');
	}
	
	isAllSet()
});

$(document).delegate(".instrument", "click", function() {
	isAllSet();
});

$(document).delegate("#oferta", "keyup", function() {
	var minLength = 50;
	var length = $("#oferta").val().length;
	if(length<50){
		$("#oferta").attr("properlySet", "");
		$("#oferta_msg").css("border-width", "5px");
		$("#oferta_msg").css("background", "#EAEDF2");
		$("#oferta_msg").show("slow");
		$("#oferta_msg").text("Musisz wykorzysta� jeszcze: "+ (minLength-length) +" znak�w");
	}else{
		$("#oferta").attr("properlySet", "true");
		$("#oferta_msg").css("border-width", "0px");
		$("#oferta_msg").css("background-color", "Transparent");
		$("#oferta_msg").html('<img src="icons/OK.png">');
	}
	isAllSet();
});

$(document).delegate("#haslo", "keyup", function() {
	var pattern = new RegExp("^(?![0-9]{6})[0-9a-zA-Z]{6}$");
	if(!pattern.test($("#haslo").val())){
		$("#haslo").attr("properlySet", "");
		$("#haslo_msg").css("border-width", "5px");
		$("#haslo_msg").css("background", "#EAEDF2");
		$("#haslo_msg").show("slow");
		$("#haslo_msg").html("chas�o musi si� zk�ada� z 6 znak�w <br>i zawiera� przynajmniej jedn� liter�");
	}else{
		$("#haslo").attr("properlySet", "true");
		$("#haslo_msg").css("border-width", "0px");
		$("#haslo_msg").css("background-color", "Transparent");
		$("#haslo_msg").html('<img src="icons/OK.png">');
	}
	isAllSet();
});

$(document).delegate("#haslo2", "keyup", function() {
	
	if( $("#haslo").val() == $("#haslo2").val()){
		$("#haslo2").attr("properlySet", "true");
		$("#haslo2_msg").css("border-width", "0px");
		$("#haslo2_msg").css("background-color", "Transparent");
		$("#haslo2_msg").html('<img src="icons/OK.png">');
	}else{
		$("#haslo2").attr("properlySet", "");
		$("#haslo2_msg").css("border-width", "5px");
		$("#haslo2_msg").css("background", "#EAEDF2");
		$("#haslo2_msg").show("slow");
		$("#haslo2_msg").html("chas�a musz� by� identyczne");
	}
	isAllSet();
});

$(document).delegate("#registerReset", "click", function() {
	$("#registerButton").hide("slow");
});

$(document).delegate("#registerButton", "click", function() { 
	
	$.ajax({
		type: "POST",
		url: "http://localhost/spawim/register",
		data: $("form").serialize()+"&instruments="+getInstruments(),
		success: function(result){
			if(result == "ok"){
				$("#messages").html("Gratulacje zosta�e� zarejestrowany.<br> Za chwilk� wrucisz do strony logowania");
				$("#messages").show("slow");
				// tu mo�na by wrzuci� zawis co lokalnej bazy danych
				setTimeout(function(){
					$("#content").load('views/login.html');
				}, 3000);
			}
		}
	});
});

function isInstrumentsSet(){
	var checkedValues = $('.instrument:checked').map(function() {
	    return this.value;
	}).get();
	
	if(checkedValues.length>0){
		return true;
	}else{
		return false;
	}
}

function getInstruments(){
	var checkedValues = $('.instrument:checked').map(function() {
	    return this.value;
	}).get();
	
	return checkedValues;
}

function isAllSet(){
	var login = Boolean($("#login").attr("properlySet"));
	var instrumenty = isInstrumentsSet();
	var oferta = Boolean($("#oferta").attr("properlySet"));
	var haslo = Boolean($("#haslo").attr("properlySet"));
	var haslo2 = Boolean($("#haslo2").attr("properlySet"));
	
	if($("#typ").val() == "nauczyciel"){
		if(login && instrumenty && oferta && haslo && haslo2){
			$("#registerButton").show("slow");
		}else{
			$("#registerButton").hide("slow");
		}
	}else if($("#typ").val() == "uczen"){
		if(login && haslo && haslo2){
			$("#registerButton").show("slow");
		}
		else{
			$("#registerButton").hide("slow");
		}
	}else{
		$("#registerButton").hide("slow");
	}
}


